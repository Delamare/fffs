use anyhow::Result;
use clap::{Parser, ValueEnum};
use fuse_mt::{
    DirectoryEntry, FileAttr, FileType, FilesystemMT, RequestInfo, ResultEntry, ResultOpen,
    ResultReaddir,
};
use lazy_static::lazy_static;
use log::{debug, error, info};
use std::ffi::OsStr;
use std::path::{Path, PathBuf};
use std::time::{Duration, SystemTime};

#[derive(Debug, Clone, ValueEnum)]
enum PassthroughMode {
    Never,
    Exact,
}

#[derive(Debug, Clone, ValueEnum)]
enum ResolveMode {
    Exact,
    Higher,
    Lower,
}

#[derive(Debug, Clone, ValueEnum)]
enum OperationMode {
    Explore,
    Preset,
}

#[derive(Debug, Parser)]
struct Settings {
    source_root: PathBuf,
    mount_point: PathBuf,
    #[clap(short, long, default_value = "never")]
    passthrough: PassthroughMode,
    #[clap(short, long)]
    /// If the container has multiple video streams, prefer this resolution
    resolution: Option<String>,
    #[clap(long, default_value = "lower")]
    /// How to resolve which video stream to pick based on the --resolution option
    resolve: ResolveMode,
    #[clap(long, default_value = "2.0")]
    channels: String,
    #[clap(long, default_value = "flac")]
    codec: String,
    #[clap(long, default_value = "mkv")]
    container: String,
    #[clap(short, long, default_value = "explore")]
    mode: OperationMode,
    #[clap(long)]
    auto_mount: bool,
    #[clap(long)]
    allow_root: bool,
    #[clap(short, long, default_value = "/tmp/fffs")]
    temp: PathBuf, // TODO: Make Option<> and use some platform abstracted TempDir crate.
}

impl Settings {
    fn get_source(&self, path: &Path) -> PathBuf {
        self.source_root
            .join(path.strip_prefix("/").unwrap_or(path))
    }
}

lazy_static! {
    static ref SETTINGS: Settings = Settings::parse();
    static ref UNSUPPORTED: i32 = std::io::Error::from(std::io::ErrorKind::Unsupported)
        .raw_os_error()
        .unwrap_or(5);
}

const TTL: Duration = Duration::from_secs(1);

fn io_error_code(e: std::io::Error) -> i32 {
    match e.raw_os_error() {
        None => {
            error!("Failed to translate FS error, using 'Unsupported' (or 5 if that fails too).");
            *UNSUPPORTED
        }
        Some(e) => e,
    }
}

struct FfFs;

impl FilesystemMT for FfFs {
    fn getattr(&self, _req: RequestInfo, path: &Path, _fh: Option<u64>) -> ResultEntry {
        debug!("getattr: {:?}", path);
        let source_path = SETTINGS.get_source(path);
        let metadata = match std::fs::metadata(&source_path) {
            Ok(metadata) => metadata,
            Err(e) => {
                error!("Failed to get metadata of source_path: {:?}", &source_path);
                return Err(io_error_code(e));
            }
        };
        Ok((
            TTL,
            FileAttr {
                size: metadata.len(),      // FIXME: the size of the ff-ed file.
                blocks: 1,                 // TODO: cross platform support
                atime: SystemTime::now(),  // TODO: cross platform support
                mtime: SystemTime::now(),  // TODO: cross platform support
                ctime: SystemTime::now(),  // TODO: cross platform support
                crtime: SystemTime::now(), // TODO: cross platform support
                kind: {
                    if metadata.is_dir() {
                        FileType::Directory
                    } else if metadata.is_file() {
                        FileType::RegularFile
                    } else if metadata.is_symlink() {
                        FileType::Symlink // TODO: follow
                    } else {
                        error!("File kind is unsupported.");
                        return Err(*UNSUPPORTED);
                    }
                },
                perm: 444,
                nlink: 1, // FIXME: better attrs
                uid: 0,   // FIXME: better attrs
                gid: 0,   // FIXME: better attrs
                rdev: 0,  // FIXME: better attrs
                flags: 0, // FIXME: better attrs
            },
        ))
    }
    fn opendir(&self, _req: RequestInfo, _path: &Path, _flags: u32) -> ResultOpen {
        Ok((666, 666))
    }
    fn readdir(&self, _req: RequestInfo, path: &Path, _fh: u64) -> ResultReaddir {
        debug!("readdir: {:?}", path);
        let source_path = SETTINGS.get_source(path);
        debug!("readdir's source_path: {:?}", &source_path);
        let dirs = match std::fs::read_dir(&source_path) {
            Ok(dirs) => dirs,
            Err(e) => {
                error!("Failed to get read_dir: {:?}", &source_path);
                return Err(io_error_code(e));
            }
        };
        Ok(dirs
            .filter_map(|entry| entry.ok())
            .map(|entry| {
                let metadata = match entry.metadata() {
                    Ok(metadata) => metadata,
                    Err(e) => {
                        error!("Failed to get metadata of source_path: {:?}", &entry);
                        return Err(io_error_code(e));
                    }
                };
                Ok(DirectoryEntry {
                    name: entry.file_name(),
                    kind: if metadata.is_dir() {
                        FileType::Directory
                    } else if metadata.is_file() {
                        FileType::RegularFile
                    } else {
                        error!("File type not supported: {:?}", &entry);
                        return Err(*UNSUPPORTED);
                    },
                })
            })
            .filter_map(|entry| entry.ok())
            .collect())
    }
}

fn setup_logger() {
    let mut builder = env_logger::Builder::from_default_env();
    // I like INFO being the default log level instead of WARN
    if std::env::var("RUST_LOG").is_err() {
        builder.filter_level(log::LevelFilter::Info);
    }
    builder.init();
}

fn main() -> Result<()> {
    setup_logger();
    info!("\n{:#?}", *SETTINGS);

    fuse_mt::mount(
        fuse_mt::FuseMT::new(FfFs, 1),
        &SETTINGS.mount_point,
        &[OsStr::new("-o"), OsStr::new("fsname=fffs")],
    )?;
    debug!("FS app exit.");
    Ok(())
}
